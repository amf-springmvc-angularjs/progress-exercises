
/*{procedures/testing/h-ttOline.i}

DEFINE BROWSE OlineBrowse QUERY OlineQuery NO-LOCK.
        DISPLAY
            ttOline.Ordernum
            ttOline.LineNum LABEL "Line"
            ttOline.ItemNum LABEL "Item" FORMAT "ZZZ9"
            ttOline.ItemName FORMAT "x(20)"
            ttOline.TotalWeight
            ttOline.Price FORMAT "ZZ,ZZ9.99"
            ttOline.Qty
            ttOline.Discount
            ttOline.ExtendedPrice LABEL "Ext.Price" FORMAT "ZZZ,ZZ9.99"
        WITH NO-ROW-MARKERS SEPARATORS 7 DOWN ROW-HEIGHT-CHARS .57.
      */  
      