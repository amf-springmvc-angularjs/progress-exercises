
/*------------------------------------------------------------------------
    File        : h-ttOline.i
    Purpose     : 

    Syntax      :

    Description : 

    Author(s)   : 
    Created     : Thu May 19 23:59:28 BRT 2016
    Notes       :
  ----------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */


/* ********************  Preprocessor Definitions  ******************** */
/* h-ttOline.i -- include file to define ttOline temp-table. */
DEFINE TEMP-TABLE ttOline LIKE order-line
FIELD ItemName LIKE item.Description
FIELD TotalWeight AS DECIMAL LABEL "Tot.Wgt.".
/* ***************************  Main Block  *************************** */
